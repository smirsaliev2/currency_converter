import React, { useState } from "react";
import Currency from './Currency'
import AddCurrency from './AddCurrency'
import fx from "money";

export default function CurrencyConverter({ rates }) {
  const [activeCurrencies, setActiveCurrencies] = useState([])
  const [isModal, setIsModal ] = useState(false) // выбор валют

  const activeCurrenciesList = activeCurrencies.map(currency => (
    <Currency 
      key={currency.name} 
      currency={currency} 
      handleInput={updateActiveCurrencies}
      removeCurrency={removeCurrency}
    />
  ))
  
  // Обновляет значения курсов при изменении инпута
  function updateActiveCurrencies(inputCurrency, value) {
    const isNumber = /^\d*\.?\d*$/.test(value)
    if (!isNumber) {
      return
    }
    setActiveCurrencies(prevState => prevState.map(cur => {
      if (inputCurrency !== cur.name) {
        const newValue = converter(inputCurrency, cur.name, Number(value))
        const updatedCur = { name: cur.name, value: newValue}
        return updatedCur
      } else {
        return { name: inputCurrency, value}
      }
    })) 
  }

  // Добавляем курс в активные
  function addCurrency( name ) {
    if (activeCurrencies.find(cur => cur.name === name)) {
      return
    }
    if (activeCurrencies.length > 0) {
      const firstCurrency = activeCurrencies[0]
      // проверяем, что у первого курса есть значение
      const newValue = firstCurrency.value 
        ? converter(firstCurrency.name, name, firstCurrency.value)
        : ''
      setActiveCurrencies(prevState => 
        [ 
          ...prevState, 
          { name, value: newValue } 
        ]
      )
    } else {
      setActiveCurrencies([ { name: name, value: '0.00' }])
    }
  }

  function removeCurrency( name ) {
    setActiveCurrencies(prevState => prevState.filter(cur =>
      cur.name !== name ))
  }

  // конвертирует курс валюты
  function converter(from, to, amount) {
    const convertedCur = fx(amount).from(from).to(to).toFixed(2)
    return convertedCur
  }

  return (
    <div className="relative rounded-lg bg-white p-4 flex flex-col">
      <div className="activeCurrenciesList">
        { activeCurrenciesList }
      </div>

      <div className="text-center">
        <span className='text-blue-600 hover:text-blue-400 hover:cursor-pointer' 
          onClick={() => {setIsModal(prevState => !prevState)}}
        >
          {
            isModal ? 'Скрыть список' : '+ Добавить валюту'
          }
        </span>
      </div>

      {
        rates && isModal && 
        <AddCurrency rates={rates} addCurrency={addCurrency}/>
      }
    </div>
  )
}