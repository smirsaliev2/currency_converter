import React from "react";

export default function Currency({ currency, handleInput, removeCurrency }) {
  return (
    <div className="Currency mb-2">
      <label className="Currency__label inline-block w-2/12 text-center"
        htmlFor={currency.name}
      >
        { currency.name }
      </label>

      <input 
        className="Currency__input inline-block rounded-lg bg-gray-100 px-4 py-1 w-8/12 focus:outline-none focus:ring" 
        value={currency.value} 
        onChange={e =>handleInput(currency.name, e.target.value)}
        id={currency.name}
      >  
      </input>
      
      <div className="Currency__remove inline-block w-2/12 text-center"
        onClick={() => removeCurrency(currency.name)}>
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 inline-block stroke-rose-500 hover:stroke-rose-300">
          <path strokeLinecap="round" strokeLinejoin="round" d="M12 9.75L14.25 12m0 0l2.25 2.25M14.25 12l2.25-2.25M14.25 12L12 14.25m-2.58 4.92l-6.375-6.375a1.125 1.125 0 010-1.59L9.42 4.83c.211-.211.498-.33.796-.33H19.5a2.25 2.25 0 012.25 2.25v10.5a2.25 2.25 0 01-2.25 2.25h-9.284c-.298 0-.585-.119-.796-.33z" />
        </svg>
      </div>
    </div>
  )
}