import React, { useState } from "react";


export default function AddCurrency( { rates, addCurrency } ) {
  const [ searchInput, setSearchInput ] = useState('')
  const [ ratesArray, setRatesArray ] = useState(rates)

  const currenciesList = ratesArray.map(rate => 
    <div className="hover:bg-blue-100 hover:cursor-pointer rounded-lg bg-white"
      key={rate.ID}
    >
      <div className='mx-2 py-2'  
        onClick={() => addCurrency(rate.CharCode)}
      >
        <div className="inline-block bg-gray-200 rounded-lg px-1 py-0.5 mr-1" >  
          {rate.CharCode}
        </div> 
        {rate.Name}
      </div>

      <hr className="mx-1"/>
    </div>
  )

  function handleChange(e) {
    const inputValue = e.target.value
    setSearchInput(inputValue)
    setRatesArray(rates.filter(rate => 
      rate.Name.toLowerCase().includes(inputValue.toLowerCase())
    ))
  }

  return (
    <div className="AddCurrency w-full max-h-96 overflow-y-scroll absolute left-0 top-full mt-2 rounded-lg p-4 bg-gray-100">
      <div className="search mb-2  bg-white rounded-lg relative">
        <input className="inline-block rounded-lg py-1 w-full focus:outline-none focus:ring indent-10" 
          id='search-inp'
          placeholder='Искать валюты'
          onChange={handleChange}
          value={searchInput}
        >
        </input>
        <label htmlFor="search-inp">
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="h-5 w-1/12 absolute left-2 inline-block top-1.5">
            <path strokeLinecap="round" strokeLinejoin="round" d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z" />
          </svg>
        </label>

      </div>

      { currenciesList }
    </div>
  )
}