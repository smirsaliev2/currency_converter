import React from "react";
import CurrencyConverter from '../components/CurrencyConverter'
import useRates from '../hooks/useRates'

export default function ConverterPage() {
  const [rates, date] = useRates()
  return (
    <div className="ConverterPage bg-gradient-to-r from-teal-600 to-cyan-500 min-h-screen">
      <div className="mx-auto max-w-md px-3 pt-10">
        <h1 className="text-white text-2xl font-bold text-center drop-shadow-lg">
          Узнать курсы валют
        </h1>
        <h3 className="mb-2 text-lg text-white text-center drop-shadow-lg">
          (Данные ЦБ РФ на {new Date(date).toLocaleDateString()})
        </h3>
        <div className="CurrencyConverter-wrapper ">
          <CurrencyConverter rates={rates}/>
        </div>
      </div>
    </div>
  )
} 