import './App.css';
import ConverterPage from './Pages/ConverterPage'

function App() {
  return (
    <div className="App">
      <ConverterPage />
    </div>
  );
}

export default App;
