import { useEffect, useState } from 'react'
import fx from 'money';

const ruble = {
  ID: 1,
  CharCode: 'RUB',
  Name: 'Российский рубль',
  Value: 1
}
// Делает 2 fetch запроса: курсы к рублю и данные о валютах
// 1. Сохраняет курсы в fx 
// 2. Сохраняет данные о валютах - в state и возвращает хуком.
// 3. Сохраняет дату запроса - в state и возвращает хуком.
export default function useRates() {
  const [ rates, setRates ] = useState()
  const [ date, setDate ] = useState()

  useEffect(() => {
    // Расширенная информация о валютах
    fetch('https://www.cbr-xml-daily.ru/daily_json.js')
    .then(response => response.json())
    .then(data => {
      const fetchedRates = [ruble]
      for (let rate in data.Valute) {
        fetchedRates.push(data.Valute[rate])
      }
      setRates(fetchedRates)
      setDate(data.Date)
    })
    // Соотношение курсов
    fetch('https://www.cbr-xml-daily.ru/latest.js')
    .then(response => response.json())
    .then(data => {
      const rates = {...data.rates, RUB: 1}
      fx.base = "RUB";
      fx.rates = rates
    })
  }, [])

  return [rates, date]
}